# wilcoxOmegaWallFunction
compatibility : OpenFoam 1812plus

Features
--------

Boundary conditions from Wilcox for k-omega turbulence models

Installation
------------

```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/bonamyc/wilcoxomegawallfunction
cd wilcoxomegawallfunction
./Allwclean
./Allwmake
```
To compile :

wmake libso

 
Usage
-----

To use the new BC : wilcoxOmegaWallFunction
Just add in system controldict the following lines :

libs (
"libmyturbulenceModels.so"
     );

Test
----

A benchmark is provided in 1DMansour:
1) Type Allrun, wait for the run to be completed 
2) Compute the wall shear stress using the postProcess utility:
pimpleFoam -postProcess -fields "(p U)" -func wallShearStress
3) Go to the python folder 'Py' and run  the script 'plot_1DMansour.py'

